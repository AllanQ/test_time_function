#!/usr/bin/env ruby
require 'benchmark'
require 'curb'
require 'nokogiri'
require 'pry'

puts"\e[H\e[2J"

page_content = Nokogiri::HTML(open('page_desktop.html'))

array = [
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['', ''],
    ['']
]

array = [
    ['', 'main'],
    ['', 'layout layout_type_maya'],
    ['', 'layout layout_type_search'],
    ['', 'layout__col layout__col_search-results_normal i-bem'],
    ['', 'filter-applied-results i-bem'],
    ['', 'snippet-list metrika i-bem snippet-list_type_vertical island'],
    ['', 'snippet-card shop-history    snippet-list__item i-bem clearfix'],
    ['', 'snippet-card__info '],
    ['', 'delivery i-bem'],
    ['', 'delivery__popup'],
    ['', 'delivery__popup-info'],
]

path_array = ['/html/body']
length_array = [0]
l = array.length

l.times do |i|
  path_array_new = []
  length_array_new = []
  case array[i].length
    when 1
      n = 1
      tag = array[i][0]
    when 2
      n = 2
      if array[i][0] == ''
        tag = 'div'
      else
        tag = array[i][0]
      end
      if array[i][1] == ''
        tag_class = ''
      else
        tag_class = "[@class='#{array[i][1]}']"
      end
    when 3
      n = 4
      tag = 'div' if array[i][0] == ''
      if array[i][1] == ''
        tag_class = ''
      else
        tag_class = "[@class='#{array[i][1]}']"
      end
      tag_position = array[i][2]
  end
  j = 0
  n.times do
    j += 1
    case j
      when 1
        str = "/#{tag}"
        str_l = 0
      when 2
        str = "/#{tag}#{tag_class}"
        str_l = 2
      when 3
        str = "/#{tag}#{tag_position}"
        str_l = 1
      when 4
        str = "/#{tag}#{tag_class}#{tag_position}"
        str_l = 3
    end
    len = path_array.length
    len.times do |k|
      path_array_new << "#{path_array[k]}#{str}"
      length_array_new << length_array[k] + str_l
    end
  end
  path_array = path_array_new
  length_array = length_array_new
end



swap = true
size = length_array.length - 1
while swap
  swap = false
  for i in 0...size
    swap |= length_array[i] > length_array[i + 1]
    if length_array[i] > length_array[i + 1]
      length_array[i], length_array[i+1] = length_array[i + 1], length_array[i]
      path_array[i], path_array[i+1] = path_array[i + 1], path_array[i]
    end
  end
  size -= 1
end

p best_path = path_array[-1]
p rigth_content = page_content.xpath(best_path).text #.text  .map(&:text)

iterations = 100

t = Time.now
iterations.times do
  page_content.xpath(best_path).map(&:text)  #.text  .map(&:text)
end
p min_time = Time.now - t
le = length_array[-1]
l = path_array.length + 1
l.times do |i|
  break if length_array[i] > le + 5
  # puts i
  # puts path_array[i]
  # puts length_array[i]
  content = page_content.xpath(path_array[i]).text #.text  .map(&:text)
  if content == rigth_content
    t = Time.now
    iterations.times do
      page_content.xpath(path_array[i]).map(&:text)  #.text  .map(&:text)
    end
    # puts
    time = Time.now - t
    if time < min_time
      min_time = time
      best_path = path_array[i]
      le = length_array[i]
    end
  end
end

puts '=' * 10
puts best_path
puts min_time
#print 'le = '
#puts le
